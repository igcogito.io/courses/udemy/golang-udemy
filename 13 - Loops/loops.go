package main

import (
	"fmt"
	"time"
)

func main () {
	i := 0

	for i < 1 {
		i++
		fmt.Println("Incrmentando i")
		time.Sleep(time.Second)
	}

	fmt.Println(i)

	for j := 0; j < 1 ; j++ {
		fmt.Println("Incrementando j", j)
		time.Sleep(time.Second)
	}

	nomes := [3]string{"João", "Davi", "Lucas"}

	for indice, nome := range nomes {
		fmt.Println(indice, nome)
	}
}