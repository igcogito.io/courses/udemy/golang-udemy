package main

import "fmt"

func main() {
// ARITMÉTICOS
	soma := 1 + 2
	subtracao := 2 - 1
	divisao := 10 / 4
	multiplicacao := 10 * 5
	restoDaDivisao := 10 % 2

	fmt.Println(soma, subtracao, divisao, multiplicacao, restoDaDivisao)

	var numero1 int = 10
	var numero2 int = 25
	somar := numero1 + numero2
	fmt.Println(somar)
// FIM DOS ARITMÉTICOS

//ATRIBUIÇÃO
	var variavel1 string = "string"
	variavel2 := "string2"
	fmt.Println(variavel1, variavel2)
// FIM DOS OPERADORES DE ATRIBUIÇÃO

//OPERADORES RELACIONAIS
	fmt.Println(1 > 2)
	fmt.Println(1 >= 2)
	fmt.Println(1 == 2)
	fmt.Println(1 <= 2)
	fmt.Println(1 < 2)
	fmt.Println(1 != 2)
// FIM DOS RELACIONAIS

//OPERADORES LÓGICOS
	fmt.Println("---------------------")
	verdadeiro, falso := true, false
	fmt.Println(verdadeiro && falso)
	fmt.Println(verdadeiro || falso)
	fmt.Println(!verdadeiro)
// FIM DOS OPERADORES LÓGICOS

// OPERADORES UNÁRIOS
	numero := 10
	numero++
	numero += 15
	fmt.Println(numero)
// FIM DOS OPERADORES UNÁRIOS

	var texto string
	if numero > 50 {
		texto = "Maior que 5"
	} else {
		texto = "Menor que 5"
	}
	fmt.Println(texto)

}
	