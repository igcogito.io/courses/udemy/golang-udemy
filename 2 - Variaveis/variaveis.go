package main

import "fmt"

func main() {
	var variavel1 string = "Variavel 1"
	variavel2 := "Variavel 2"
	fmt.Println(variavel1)
	fmt.Println(variavel2)


var (
	variavel3 string = "Man United"
	variavel4 string = "Man City"
)

fmt.Println(variavel3, variavel4)
}