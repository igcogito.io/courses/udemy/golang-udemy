package main

import "fmt"

type usuario struct {
	nome string
	idade int
	
}

func main() {
	fmt.Println("Arquivo structs")

	var u usuario
	u.nome = "Igor"
	u.idade = 22
	fmt.Println(u)

	usuario2 := usuario{"Kath", 21}
	fmt.Println(usuario2)

	usuario3 := usuario{nome: "Diego"}
	fmt.Println(usuario3)
}