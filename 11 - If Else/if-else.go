package main

import "fmt"

func main() {
	fmt.Println("Estrutura de controle")

	numero := 10

	if numero > 15 {
		fmt.Println("Maior que 15")
	} else {
		fmt.Println("Menor ou igual que 15")
	}

	if numero2 := -5; numero2 > 0 {
		fmt.Println("Maior que 0")
	} else if numero2 < -10 {
		fmt.Println("numero é menor que -10")	
	} else {
		fmt. Println("entre 0 e -10")
	}
	
}