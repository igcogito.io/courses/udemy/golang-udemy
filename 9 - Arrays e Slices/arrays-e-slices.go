package main

import "fmt"

func main() {
	fmt.Println("Arrays e Slices")

	var array1 [5] int
	array1[0] = 10
	array1[1] = 20
	array1[2] = 30
	array1[3] = 40
	array1[4] = 50
	fmt.Println(array1)

// 	OU
	array2 := [5] int{10, 20, 30, 40, 50}
	fmt.Println(array2)

// Os "..." deixa o tamanho baseado na qtdade de valores q eu passei - não é mt utilizado
	array3 := [...]string{"Posição1", "Posição 2", "Posição 3", "Posição 4", "Posição 5"}
	fmt.Println(array3)

// SLICE = não preciso me preocupar com o tamanhao (flexivel) - mt utilizado
	slice := []int {1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(slice)

// adicionar valor no slice
	slice = append(slice, 10)
	fmt.Println(slice)

// pegar uma fatia de algum array com slice

	slice2 := array3[1:3]
	fmt.Println(slice2)

// Alterar valor de alguma posição no array
	array3[0] = "Mudei o valor"
	fmt.Println(array3)

//Arrays Internos
	slice3 := make([]float32, 10, 11)
	fmt.Println(slice3)

	slice3 = append(slice3, 5)
	slice3 = append(slice3, 6)
	fmt.Println(slice3)


	slice4 := make([]float32, 5)
	fmt.Println(slice4)
	slice4 = append(slice4, 10)
	fmt.Println(len(slice))
	fmt.Println(cap(slice4))
}
