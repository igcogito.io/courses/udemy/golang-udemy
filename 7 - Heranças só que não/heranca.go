///////////////// O MAIS PROXIMO QUE GOLANG CHEGA DE HERANÇA/////////////
package main

import "fmt"

type pessoa struct {
	nome string
	sobrenome string
	idade int
	altura int 
}

type estudante struct {
	pessoa pessoa
	faculdade string
	curso string
}

func main() {
	fmt.Println("Hernaça")

	p1 := pessoa{"João", "Pedro", 20, 178}
	fmt.Println(p1)

	e1 := estudante{p1, "T.I", "UAM"}
	fmt.Println(e1)
}
///////////////// O MAIS PROXIMO QUE GOLANG CHEGA DE HERANÇA/////////////
